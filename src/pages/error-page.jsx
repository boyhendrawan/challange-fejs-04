import { useRouteError } from "react-router-dom";

export default function ErrorPage() {
  const error = useRouteError();
  console.error(error);

  return (
    <div style={{display:"grid",gridColumn:"1fr",justifyContent:"center",alignItems:"center",height:"100vh"}}>
        <div>

            <h1 >Oops! {error.status} page {error.statusText || error.message} </h1>
            <p>Sorry, an unexpected error has occurred.</p>
            <p>{error.data}</p>
        </div>
      
    </div>
  );
}