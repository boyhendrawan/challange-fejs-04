import React from "react";
import {Route,Routes} from "react-router-dom";
import Home from "../pages/Home";
import Detail from "../pages/Detail";
import Catalog from "../pages/Catalog";

const Routers = ()=>{
    return(
    <Routes>
        <Route 
        path="/:category/search/:keyword" 
        Component={Catalog}/>
        <Route 
        path="/:category/:id" 
        Component={Detail}/>
        <Route 
        path="/:category/detail" 
        Component={Catalog}/>
        <Route 
        path="/:category" 
        Component={Catalog}/>
        <Route 
        path="/"
        Component={Home}/>
    </Routes>
    );
}
export default Routers;