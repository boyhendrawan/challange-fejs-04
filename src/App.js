import "./App.scss";
import "swiper/swiper.min.css";
import "./assets/boxicons-2.0.7/css/boxicons.min.css";
import React from "react";
import { createBrowserRouter,Outlet,RouterProvider} from "react-router-dom";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import Catalog from "./pages/Catalog";
import Home from "./pages/Home";
import Detail from "./pages/detail/Detail";
import ErrorPage from "./pages/error-page";


const App = () => {
  const Layout=() =>{
    return(
      <>
      <Header/>
      <Outlet/>
      <Footer/>
      </>
    )
  }
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Layout />,
      errorElement:<ErrorPage/>,
      children: [
        {
          path: "/",
          element:<Home/>
        },
        {
          path: "/:category",
          element:<Catalog/>
        },
        {
          path: "/:category/:id",
          element:<Detail/>
        },
        
        
      ],
    },
  ]);

  return (
    <RouterProvider router={router} />
  )
};
export default App;
